import pygame
import math

# starting point
turtle_x = 0
turtle_y = 0
turtle_angle = 0

# line properties
line_color = (0, 0, 0)
line_thickness = 1

# screen size
screenWidth, screenHeight = 640, 480

# initialization
pygame.init()
screen = pygame.display.set_mode((screenWidth, screenHeight))
screen.fill((255,255,255))
pygame.display.flip()


def turn(angle):
    global turtle_angle
    turtle_angle += angle * math.pi / 180


def move(distance):
    global turtle_x, turtle_y
    old_y = turtle_y
    old_x = turtle_x
    turtle_y -= distance * math.sin(turtle_angle)
    turtle_x += distance * math.cos(turtle_angle)
    pygame.draw.line(screen, line_color, (old_x, old_y), (turtle_x, turtle_y), line_thickness)
    pygame.display.flip()


# your code here:
turn(-30)
move(100)
turn(-30)
move(100)
turn(40)
move(100)
turn(20)
move(100)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
